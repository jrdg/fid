﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace vscode_playground
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class DailyPriceBySeries
    {
        public List<decimal?> A { get; set; }
        public List<decimal?> T5 { get; set; }
        public List<decimal?> T8 { get; set; }
        public List<decimal?> B { get; set; }
        public List<decimal?> S5 { get; set; }
        public List<decimal?> S8 { get; set; }
        public List<decimal?> C { get; set; }
        public List<decimal?> D { get; set; }
        public List<decimal?> E1 { get; set; }
        public List<decimal?> E2 { get; set; }
        public List<decimal?> E3 { get; set; }
        public List<decimal?> E4 { get; set; }
        public List<decimal?> E5 { get; set; }
        public List<decimal?> E1T5 { get; set; }
        public List<decimal?> E2T5 { get; set; }
        public List<decimal?> E3T5 { get; set; }
        public List<decimal?> E4T5 { get; set; }
        public List<decimal?> E5T5 { get; set; }
        public List<decimal?> F { get; set; }
        public List<decimal?> F5 { get; set; }
        public List<decimal?> F8 { get; set; }
        public List<decimal?> I { get; set; }
        public List<decimal?> I5 { get; set; }
        public List<decimal?> I8 { get; set; }
        public List<decimal?> L { get; set; }
        public List<decimal?> P2 { get; set; }
        public List<decimal?> P3T5 { get; set; }
        public List<decimal?> P2T5 { get; set; }
        public List<decimal?> P1 { get; set; }
        public List<decimal?> P3 { get; set; }
        public List<decimal?> P4 { get; set; }
        public List<decimal?> P4T5 { get; set; }
        public List<decimal?> P1T5 { get; set; }
        public List<decimal?> P5 { get; set; }
    }

    public class DistributionsBySeries
    {
        // c'est une list de key pair value exemple : [{ "date": 0.2324 }, { "date": 0.2324 }, { "date": 0.2324 }, { "date": 0.2324 }]
        public List<object> A { get; set; }
        public List<object> T5 { get; set; }
        public List<object> T8 { get; set; }
        public List<object> B { get; set; }
        public List<object> S5 { get; set; }
        public List<object> S8 { get; set; }
        public List<object> C { get; set; }
        public List<object> D { get; set; }
        public List<object> E1 { get; set; }
        public List<object> E2 { get; set; }
        public List<object> E3 { get; set; }
        public List<object> E4 { get; set; }
        public List<object> E5 { get; set; }
        public List<object> E1T5 { get; set; }
        public List<object> E2T5 { get; set; }
        public List<object> E3T5 { get; set; }
        public List<object> E4T5 { get; set; }
        public List<object> E5T5 { get; set; }
        public List<object> F { get; set; }
        public List<object> F5 { get; set; }
        public List<object> F8 { get; set; }
        public List<object> I { get; set; }
        public List<object> I5 { get; set; }
        public List<object> I8 { get; set; }
        public List<object> L { get; set; }
        public List<object> P2 { get; set; }
        public List<object> P3T5 { get; set; }
        public List<object> P2T5 { get; set; }
        public List<object> P1 { get; set; }
        public List<object> P3 { get; set; }
        public List<object> P4 { get; set; }
        public List<object> P4T5 { get; set; }
        public List<object> P1T5 { get; set; }
        public List<object> P5 { get; set; }
    }

    public class RootFidelityFundChart
    {
        public string asOfDate { get; set; }
        public DailyPriceBySeries dailyPriceBySeries { get; set; }
        public DistributionsBySeries distributionsBySeries { get; set; }
    }


}
