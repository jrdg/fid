// Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
using System.Collections.Generic;
using System.Text.Json.Serialization;

// Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class Codes
    {
        public object ISC { get; set; }
        public object NL { get; set; }
        public object DSC { get; set; }
        public object LL { get; set; }
        public object LL2 { get; set; }
    }

    public class E2
    {
        public object seriesCd { get; set; }
        public object axisCd { get; set; }
        public object currency { get; set; }
        public object nav { get; set; }
        public object navDt { get; set; }
        public object navChgAmt { get; set; }
        public object navChgPct { get; set; }
        public object navChgInd { get; set; }
        public object merDt { get; set; }
        public object merPct { get; set; }
        public object inceptionDt { get; set; }
        public object mstarRtng { get; set; }
        public object mstarDt { get; set; }
        public object distFreq { get; set; }
        public Codes codes { get; set; }
        public object aggrAssets { get; set; }
        public object aggrAssetsDt { get; set; }
        public object yieldNet { get; set; }
        public object yieldNetDate { get; set; }
        public object yield7Day { get; set; }
        public object yield7DayDate { get; set; }
        public object navChgYtdPct { get; set; }
        public object nav52WkHi { get; set; }
        public object nav52WkLo { get; set; }
        public object etf { get; set; }
        public object nav52WkHiDt { get; set; }
        public object nav52WkLoDt { get; set; }
        public object exchangeName { get; set; }
        public object invProgram { get; set; }
        public object rebalanceFreq { get; set; }
        public object indexProvider { get; set; }
        public object mgmtFee { get; set; }
        public object key { get; set; }
        public DisplayReturns displayReturns { get; set; }
        public object threeYearsData { get; set; }
    }

    public class E5T5
    {
        public object seriesCd { get; set; }
        public object axisCd { get; set; }
        public object currency { get; set; }
        public object nav { get; set; }
        public object navDt { get; set; }
        public object navChgAmt { get; set; }
        public object navChgPct { get; set; }
        public object navChgInd { get; set; }
        public object merDt { get; set; }
        public object merPct { get; set; }
        public object inceptionDt { get; set; }
        public object mstarRtng { get; set; }
        public object mstarDt { get; set; }
        public object distFreq { get; set; }
        public Codes codes { get; set; }
        public object aggrAssets { get; set; }
        public object aggrAssetsDt { get; set; }
        public object yieldNet { get; set; }
        public object yieldNetDate { get; set; }
        public object yield7Day { get; set; }
        public object yield7DayDate { get; set; }
        public object navChgYtdPct { get; set; }
        public object nav52WkHi { get; set; }
        public object nav52WkLo { get; set; }
        public object etf { get; set; }
        public object nav52WkHiDt { get; set; }
        public object nav52WkLoDt { get; set; }
        public object exchangeName { get; set; }
        public object invProgram { get; set; }
        public object rebalanceFreq { get; set; }
        public object indexProvider { get; set; }
        public object mgmtFee { get; set; }
        public object key { get; set; }
        public DisplayReturns displayReturns { get; set; }
        public object threeYearsData { get; set; }
    }

    public class P2
    {
        public object seriesCd { get; set; }
        public object axisCd { get; set; }
        public object currency { get; set; }
        public object nav { get; set; }
        public object navDt { get; set; }
        public object navChgAmt { get; set; }
        public object navChgPct { get; set; }
        public object navChgInd { get; set; }
        public object merDt { get; set; }
        public object merPct { get; set; }
        public object inceptionDt { get; set; }
        public object mstarRtng { get; set; }
        public object mstarDt { get; set; }
        public object distFreq { get; set; }
        public Codes codes { get; set; }
        public object aggrAssets { get; set; }
        public object aggrAssetsDt { get; set; }
        public object yieldNet { get; set; }
        public object yieldNetDate { get; set; }
        public object yield7Day { get; set; }
        public object yield7DayDate { get; set; }
        public object navChgYtdPct { get; set; }
        public object nav52WkHi { get; set; }
        public object nav52WkLo { get; set; }
        public object etf { get; set; }
        public object nav52WkHiDt { get; set; }
        public object nav52WkLoDt { get; set; }
        public object exchangeName { get; set; }
        public object invProgram { get; set; }
        public object rebalanceFreq { get; set; }
        public object indexProvider { get; set; }
        public object mgmtFee { get; set; }
        public object key { get; set; }
        public DisplayReturns displayReturns { get; set; }
        public object threeYearsData { get; set; }
    }

    public class E1T5
    {
        public object seriesCd { get; set; }
        public object axisCd { get; set; }
        public object currency { get; set; }
        public object nav { get; set; }
        public object navDt { get; set; }
        public object navChgAmt { get; set; }
        public object navChgPct { get; set; }
        public object navChgInd { get; set; }
        public object merDt { get; set; }
        public object merPct { get; set; }
        public object inceptionDt { get; set; }
        public object mstarRtng { get; set; }
        public object mstarDt { get; set; }
        public object distFreq { get; set; }
        public Codes codes { get; set; }
        public object aggrAssets { get; set; }
        public object aggrAssetsDt { get; set; }
        public object yieldNet { get; set; }
        public object yieldNetDate { get; set; }
        public object yield7Day { get; set; }
        public object yield7DayDate { get; set; }
        public object navChgYtdPct { get; set; }
        public object nav52WkHi { get; set; }
        public object nav52WkLo { get; set; }
        public object etf { get; set; }
        public object nav52WkHiDt { get; set; }
        public object nav52WkLoDt { get; set; }
        public object exchangeName { get; set; }
        public object invProgram { get; set; }
        public object rebalanceFreq { get; set; }
        public object indexProvider { get; set; }
        public object mgmtFee { get; set; }
        public object key { get; set; }
        public DisplayReturns displayReturns { get; set; }
        public object threeYearsData { get; set; }
    }

    public class F
    {
        public object seriesCd { get; set; }
        public object axisCd { get; set; }
        public object currency { get; set; }
        public object nav { get; set; }
        public object navDt { get; set; }
        public object navChgAmt { get; set; }
        public object navChgPct { get; set; }
        public object navChgInd { get; set; }
        public object merDt { get; set; }
        public object merPct { get; set; }
        public object inceptionDt { get; set; }
        public object mstarRtng { get; set; }
        public object mstarDt { get; set; }
        public object distFreq { get; set; }
        public Codes codes { get; set; }
        public object aggrAssets { get; set; }
        public object aggrAssetsDt { get; set; }
        public object yieldNet { get; set; }
        public object yieldNetDate { get; set; }
        public object yield7Day { get; set; }
        public object yield7DayDate { get; set; }
        public object navChgYtdPct { get; set; }
        public object nav52WkHi { get; set; }
        public object nav52WkLo { get; set; }
        public object etf { get; set; }
        public object nav52WkHiDt { get; set; }
        public object nav52WkLoDt { get; set; }
        public object exchangeName { get; set; }
        public object invProgram { get; set; }
        public object rebalanceFreq { get; set; }
        public object indexProvider { get; set; }
        public object mgmtFee { get; set; }
        public object key { get; set; }
        public DisplayReturns displayReturns { get; set; }
        public object threeYearsData { get; set; }
    }

    public class T5
    {
        public object seriesCd { get; set; }
        public object axisCd { get; set; }
        public object currency { get; set; }
        public object nav { get; set; }
        public object navDt { get; set; }
        public object navChgAmt { get; set; }
        public object navChgPct { get; set; }
        public object navChgInd { get; set; }
        public object merDt { get; set; }
        public object merPct { get; set; }
        public object inceptionDt { get; set; }
        public object mstarRtng { get; set; }
        public object mstarDt { get; set; }
        public object distFreq { get; set; }
        public Codes codes { get; set; }
        public object aggrAssets { get; set; }
        public object aggrAssetsDt { get; set; }
        public object yieldNet { get; set; }
        public object yieldNetDate { get; set; }
        public object yield7Day { get; set; }
        public object yield7DayDate { get; set; }
        public object navChgYtdPct { get; set; }
        public object nav52WkHi { get; set; }
        public object nav52WkLo { get; set; }
        public object etf { get; set; }
        public object nav52WkHiDt { get; set; }
        public object nav52WkLoDt { get; set; }
        public object exchangeName { get; set; }
        public object invProgram { get; set; }
        public object rebalanceFreq { get; set; }
        public object indexProvider { get; set; }
        public object mgmtFee { get; set; }
        public object key { get; set; }
        public DisplayReturns displayReturns { get; set; }
        public object threeYearsData { get; set; }
    }

    public class E5
    {
        public object seriesCd { get; set; }
        public object axisCd { get; set; }
        public object currency { get; set; }
        public object nav { get; set; }
        public object navDt { get; set; }
        public object navChgAmt { get; set; }
        public object navChgPct { get; set; }
        public object navChgInd { get; set; }
        public object merDt { get; set; }
        public object merPct { get; set; }
        public object inceptionDt { get; set; }
        public object mstarRtng { get; set; }
        public object mstarDt { get; set; }
        public object distFreq { get; set; }
        public Codes codes { get; set; }
        public object aggrAssets { get; set; }
        public object aggrAssetsDt { get; set; }
        public object yieldNet { get; set; }
        public object yieldNetDate { get; set; }
        public object yield7Day { get; set; }
        public object yield7DayDate { get; set; }
        public object navChgYtdPct { get; set; }
        public object nav52WkHi { get; set; }
        public object nav52WkLo { get; set; }
        public object etf { get; set; }
        public object nav52WkHiDt { get; set; }
        public object nav52WkLoDt { get; set; }
        public object exchangeName { get; set; }
        public object invProgram { get; set; }
        public object rebalanceFreq { get; set; }
        public object indexProvider { get; set; }
        public object mgmtFee { get; set; }
        public object key { get; set; }
        public DisplayReturns displayReturns { get; set; }
        public object threeYearsData { get; set; }
    }

    public class E1
    {
        public object seriesCd { get; set; }
        public object axisCd { get; set; }
        public object currency { get; set; }
        public object nav { get; set; }
        public object navDt { get; set; }
        public object navChgAmt { get; set; }
        public object navChgPct { get; set; }
        public object navChgInd { get; set; }
        public object merDt { get; set; }
        public object merPct { get; set; }
        public object inceptionDt { get; set; }
        public object mstarRtng { get; set; }
        public object mstarDt { get; set; }
        public object distFreq { get; set; }
        public Codes codes { get; set; }
        public object aggrAssets { get; set; }
        public object aggrAssetsDt { get; set; }
        public object yieldNet { get; set; }
        public object yieldNetDate { get; set; }
        public object yield7Day { get; set; }
        public object yield7DayDate { get; set; }
        public object navChgYtdPct { get; set; }
        public object nav52WkHi { get; set; }
        public object nav52WkLo { get; set; }
        public object etf { get; set; }
        public object nav52WkHiDt { get; set; }
        public object nav52WkLoDt { get; set; }
        public object exchangeName { get; set; }
        public object invProgram { get; set; }
        public object rebalanceFreq { get; set; }
        public object indexProvider { get; set; }
        public object mgmtFee { get; set; }
        public object key { get; set; }
        public DisplayReturns displayReturns { get; set; }
        public object threeYearsData { get; set; }
    }

    public class P3T5
    {
        public object seriesCd { get; set; }
        public object axisCd { get; set; }
        public object currency { get; set; }
        public object nav { get; set; }
        public object navDt { get; set; }
        public object navChgAmt { get; set; }
        public object navChgPct { get; set; }
        public object navChgInd { get; set; }
        public object merDt { get; set; }
        public object merPct { get; set; }
        public object inceptionDt { get; set; }
        public object mstarRtng { get; set; }
        public object mstarDt { get; set; }
        public object distFreq { get; set; }
        public Codes codes { get; set; }
        public object aggrAssets { get; set; }
        public object aggrAssetsDt { get; set; }
        public object yieldNet { get; set; }
        public object yieldNetDate { get; set; }
        public object yield7Day { get; set; }
        public object yield7DayDate { get; set; }
        public object navChgYtdPct { get; set; }
        public object nav52WkHi { get; set; }
        public object nav52WkLo { get; set; }
        public object etf { get; set; }
        public object nav52WkHiDt { get; set; }
        public object nav52WkLoDt { get; set; }
        public object exchangeName { get; set; }
        public object invProgram { get; set; }
        public object rebalanceFreq { get; set; }
        public object indexProvider { get; set; }
        public object mgmtFee { get; set; }
        public object key { get; set; }
        public DisplayReturns displayReturns { get; set; }
        public object threeYearsData { get; set; }
    }

    public class B
    {
        public object seriesCd { get; set; }
        public object axisCd { get; set; }
        public object currency { get; set; }
        public object nav { get; set; }
        public object navDt { get; set; }
        public object navChgAmt { get; set; }
        public object navChgPct { get; set; }
        public object navChgInd { get; set; }
        public object merDt { get; set; }
        public object merPct { get; set; }
        public object inceptionDt { get; set; }
        public object mstarRtng { get; set; }
        public object mstarDt { get; set; }
        public object distFreq { get; set; }
        public Codes codes { get; set; }
        public object aggrAssets { get; set; }
        public object aggrAssetsDt { get; set; }
        public object yieldNet { get; set; }
        public object yieldNetDate { get; set; }
        public object yield7Day { get; set; }
        public object yield7DayDate { get; set; }
        public object navChgYtdPct { get; set; }
        public object nav52WkHi { get; set; }
        public object nav52WkLo { get; set; }
        public object etf { get; set; }
        public object nav52WkHiDt { get; set; }
        public object nav52WkLoDt { get; set; }
        public object exchangeName { get; set; }
        public object invProgram { get; set; }
        public object rebalanceFreq { get; set; }
        public object indexProvider { get; set; }
        public object mgmtFee { get; set; }
        public object key { get; set; }
        public DisplayReturns displayReturns { get; set; }
        public object threeYearsData { get; set; }
    }

    public class A
    {
        public object seriesCd { get; set; }
        public object axisCd { get; set; }
        public object currency { get; set; }
        public object nav { get; set; }
        public object navDt { get; set; }
        public object navChgAmt { get; set; }
        public object navChgPct { get; set; }
        public object navChgInd { get; set; }
        public object merDt { get; set; }
        public object merPct { get; set; }
        public object inceptionDt { get; set; }
        public object mstarRtng { get; set; }
        public object mstarDt { get; set; }
        public object distFreq { get; set; }
        public Codes codes { get; set; }
        public object aggrAssets { get; set; }
        public object aggrAssetsDt { get; set; }
        public object yieldNet { get; set; }
        public object yieldNetDate { get; set; }
        public object yield7Day { get; set; }
        public object yield7DayDate { get; set; }
        public object navChgYtdPct { get; set; }
        public object nav52WkHi { get; set; }
        public object nav52WkLo { get; set; }
        public object etf { get; set; }
        public object nav52WkHiDt { get; set; }
        public object nav52WkLoDt { get; set; }
        public object exchangeName { get; set; }
        public object invProgram { get; set; }
        public object rebalanceFreq { get; set; }
        public object indexProvider { get; set; }
        public object mgmtFee { get; set; }
        public object key { get; set; }
        public DisplayReturns displayReturns { get; set; }
        public object threeYearsData { get; set; }
    }

    public class P2T5
    {
        public object seriesCd { get; set; }
        public object axisCd { get; set; }
        public object currency { get; set; }
        public object nav { get; set; }
        public object navDt { get; set; }
        public object navChgAmt { get; set; }
        public object navChgPct { get; set; }
        public object navChgInd { get; set; }
        public object merDt { get; set; }
        public object merPct { get; set; }
        public object inceptionDt { get; set; }
        public object mstarRtng { get; set; }
        public object mstarDt { get; set; }
        public object distFreq { get; set; }
        public Codes codes { get; set; }
        public object aggrAssets { get; set; }
        public object aggrAssetsDt { get; set; }
        public object yieldNet { get; set; }
        public object yieldNetDate { get; set; }
        public object yield7Day { get; set; }
        public object yield7DayDate { get; set; }
        public object navChgYtdPct { get; set; }
        public object nav52WkHi { get; set; }
        public object nav52WkLo { get; set; }
        public object etf { get; set; }
        public object nav52WkHiDt { get; set; }
        public object nav52WkLoDt { get; set; }
        public object exchangeName { get; set; }
        public object invProgram { get; set; }
        public object rebalanceFreq { get; set; }
        public object indexProvider { get; set; }
        public object mgmtFee { get; set; }
        public object key { get; set; }
        public DisplayReturns displayReturns { get; set; }
        public object threeYearsData { get; set; }
    }

    public class E4
    {
        public object seriesCd { get; set; }
        public object axisCd { get; set; }
        public object currency { get; set; }
        public object nav { get; set; }
        public object navDt { get; set; }
        public object navChgAmt { get; set; }
        public object navChgPct { get; set; }
        public object navChgInd { get; set; }
        public object merDt { get; set; }
        public object merPct { get; set; }
        public object inceptionDt { get; set; }
        public object mstarRtng { get; set; }
        public object mstarDt { get; set; }
        public object distFreq { get; set; }
        public Codes codes { get; set; }
        public object aggrAssets { get; set; }
        public object aggrAssetsDt { get; set; }
        public object yieldNet { get; set; }
        public object yieldNetDate { get; set; }
        public object yield7Day { get; set; }
        public object yield7DayDate { get; set; }
        public object navChgYtdPct { get; set; }
        public object nav52WkHi { get; set; }
        public object nav52WkLo { get; set; }
        public object etf { get; set; }
        public object nav52WkHiDt { get; set; }
        public object nav52WkLoDt { get; set; }
        public object exchangeName { get; set; }
        public object invProgram { get; set; }
        public object rebalanceFreq { get; set; }
        public object indexProvider { get; set; }
        public object mgmtFee { get; set; }
        public object key { get; set; }
        public DisplayReturns displayReturns { get; set; }
        public object threeYearsData { get; set; }
    }

    public class E2T5
    {
        public object seriesCd { get; set; }
        public object axisCd { get; set; }
        public object currency { get; set; }
        public object nav { get; set; }
        public object navDt { get; set; }
        public object navChgAmt { get; set; }
        public object navChgPct { get; set; }
        public object navChgInd { get; set; }
        public object merDt { get; set; }
        public object merPct { get; set; }
        public object inceptionDt { get; set; }
        public object mstarRtng { get; set; }
        public object mstarDt { get; set; }
        public object distFreq { get; set; }
        public Codes codes { get; set; }
        public object aggrAssets { get; set; }
        public object aggrAssetsDt { get; set; }
        public object yieldNet { get; set; }
        public object yieldNetDate { get; set; }
        public object yield7Day { get; set; }
        public object yield7DayDate { get; set; }
        public object navChgYtdPct { get; set; }
        public object nav52WkHi { get; set; }
        public object nav52WkLo { get; set; }
        public object etf { get; set; }
        public object nav52WkHiDt { get; set; }
        public object nav52WkLoDt { get; set; }
        public object exchangeName { get; set; }
        public object invProgram { get; set; }
        public object rebalanceFreq { get; set; }
        public object indexProvider { get; set; }
        public object mgmtFee { get; set; }
        public object key { get; set; }
        public DisplayReturns displayReturns { get; set; }
        public object threeYearsData { get; set; }
    }

    public class P1
    {
        public object seriesCd { get; set; }
        public object axisCd { get; set; }
        public object currency { get; set; }
        public object nav { get; set; }
        public object navDt { get; set; }
        public object navChgAmt { get; set; }
        public object navChgPct { get; set; }
        public object navChgInd { get; set; }
        public object merDt { get; set; }
        public object merPct { get; set; }
        public object inceptionDt { get; set; }
        public object mstarRtng { get; set; }
        public object mstarDt { get; set; }
        public object distFreq { get; set; }
        public Codes codes { get; set; }
        public object aggrAssets { get; set; }
        public object aggrAssetsDt { get; set; }
        public object yieldNet { get; set; }
        public object yieldNetDate { get; set; }
        public object yield7Day { get; set; }
        public object yield7DayDate { get; set; }
        public object navChgYtdPct { get; set; }
        public object nav52WkHi { get; set; }
        public object nav52WkLo { get; set; }
        public object etf { get; set; }
        public object nav52WkHiDt { get; set; }
        public object nav52WkLoDt { get; set; }
        public object exchangeName { get; set; }
        public object invProgram { get; set; }
        public object rebalanceFreq { get; set; }
        public object indexProvider { get; set; }
        public object mgmtFee { get; set; }
        public object key { get; set; }
        public DisplayReturns displayReturns { get; set; }
        public object threeYearsData { get; set; }
    }

    public class P3
    {
        public object seriesCd { get; set; }
        public object axisCd { get; set; }
        public object currency { get; set; }
        public object nav { get; set; }
        public object navDt { get; set; }
        public object navChgAmt { get; set; }
        public object navChgPct { get; set; }
        public object navChgInd { get; set; }
        public object merDt { get; set; }
        public object merPct { get; set; }
        public object inceptionDt { get; set; }
        public object mstarRtng { get; set; }
        public object mstarDt { get; set; }
        public object distFreq { get; set; }
        public Codes codes { get; set; }
        public object aggrAssets { get; set; }
        public object aggrAssetsDt { get; set; }
        public object yieldNet { get; set; }
        public object yieldNetDate { get; set; }
        public object yield7Day { get; set; }
        public object yield7DayDate { get; set; }
        public object navChgYtdPct { get; set; }
        public object nav52WkHi { get; set; }
        public object nav52WkLo { get; set; }
        public object etf { get; set; }
        public object nav52WkHiDt { get; set; }
        public object nav52WkLoDt { get; set; }
        public object exchangeName { get; set; }
        public object invProgram { get; set; }
        public object rebalanceFreq { get; set; }
        public object indexProvider { get; set; }
        public object mgmtFee { get; set; }
        public object key { get; set; }
        public DisplayReturns displayReturns { get; set; }
        public object threeYearsData { get; set; }
    }

    public class F5
    {
        public object seriesCd { get; set; }
        public object axisCd { get; set; }
        public object currency { get; set; }
        public object nav { get; set; }
        public object navDt { get; set; }
        public object navChgAmt { get; set; }
        public object navChgPct { get; set; }
        public object navChgInd { get; set; }
        public object merDt { get; set; }
        public object merPct { get; set; }
        public object inceptionDt { get; set; }
        public object mstarRtng { get; set; }
        public object mstarDt { get; set; }
        public object distFreq { get; set; }
        public Codes codes { get; set; }
        public object aggrAssets { get; set; }
        public object aggrAssetsDt { get; set; }
        public object yieldNet { get; set; }
        public object yieldNetDate { get; set; }
        public object yield7Day { get; set; }
        public object yield7DayDate { get; set; }
        public object navChgYtdPct { get; set; }
        public object nav52WkHi { get; set; }
        public object nav52WkLo { get; set; }
        public object etf { get; set; }
        public object nav52WkHiDt { get; set; }
        public object nav52WkLoDt { get; set; }
        public object exchangeName { get; set; }
        public object invProgram { get; set; }
        public object rebalanceFreq { get; set; }
        public object indexProvider { get; set; }
        public object mgmtFee { get; set; }
        public object key { get; set; }
        public DisplayReturns displayReturns { get; set; }
        public object threeYearsData { get; set; }
    }

    public class E3T5
    {
        public object seriesCd { get; set; }
        public object axisCd { get; set; }
        public object currency { get; set; }
        public object nav { get; set; }
        public object navDt { get; set; }
        public object navChgAmt { get; set; }
        public object navChgPct { get; set; }
        public object navChgInd { get; set; }
        public object merDt { get; set; }
        public object merPct { get; set; }
        public object inceptionDt { get; set; }
        public object mstarRtng { get; set; }
        public object mstarDt { get; set; }
        public object distFreq { get; set; }
        public Codes codes { get; set; }
        public object aggrAssets { get; set; }
        public object aggrAssetsDt { get; set; }
        public object yieldNet { get; set; }
        public object yieldNetDate { get; set; }
        public object yield7Day { get; set; }
        public object yield7DayDate { get; set; }
        public object navChgYtdPct { get; set; }
        public object nav52WkHi { get; set; }
        public object nav52WkLo { get; set; }
        public object etf { get; set; }
        public object nav52WkHiDt { get; set; }
        public object nav52WkLoDt { get; set; }
        public object exchangeName { get; set; }
        public object invProgram { get; set; }
        public object rebalanceFreq { get; set; }
        public object indexProvider { get; set; }
        public object mgmtFee { get; set; }
        public object key { get; set; }
        public DisplayReturns displayReturns { get; set; }
        public object threeYearsData { get; set; }
    }

    public class P4
    {
        public object seriesCd { get; set; }
        public object axisCd { get; set; }
        public object currency { get; set; }
        public object nav { get; set; }
        public object navDt { get; set; }
        public object navChgAmt { get; set; }
        public object navChgPct { get; set; }
        public object navChgInd { get; set; }
        public object merDt { get; set; }
        public object merPct { get; set; }
        public object inceptionDt { get; set; }
        public object mstarRtng { get; set; }
        public object mstarDt { get; set; }
        public object distFreq { get; set; }
        public Codes codes { get; set; }
        public object aggrAssets { get; set; }
        public object aggrAssetsDt { get; set; }
        public object yieldNet { get; set; }
        public object yieldNetDate { get; set; }
        public object yield7Day { get; set; }
        public object yield7DayDate { get; set; }
        public object navChgYtdPct { get; set; }
        public object nav52WkHi { get; set; }
        public object nav52WkLo { get; set; }
        public object etf { get; set; }
        public object nav52WkHiDt { get; set; }
        public object nav52WkLoDt { get; set; }
        public object exchangeName { get; set; }
        public object invProgram { get; set; }
        public object rebalanceFreq { get; set; }
        public object indexProvider { get; set; }
        public object mgmtFee { get; set; }
        public object key { get; set; }
        public DisplayReturns displayReturns { get; set; }
        public object threeYearsData { get; set; }
    }

    public class P4T5
    {
        public object seriesCd { get; set; }
        public object axisCd { get; set; }
        public object currency { get; set; }
        public object nav { get; set; }
        public object navDt { get; set; }
        public object navChgAmt { get; set; }
        public object navChgPct { get; set; }
        public object navChgInd { get; set; }
        public object merDt { get; set; }
        public object merPct { get; set; }
        public object inceptionDt { get; set; }
        public object mstarRtng { get; set; }
        public object mstarDt { get; set; }
        public object distFreq { get; set; }
        public Codes codes { get; set; }
        public object aggrAssets { get; set; }
        public object aggrAssetsDt { get; set; }
        public object yieldNet { get; set; }
        public object yieldNetDate { get; set; }
        public object yield7Day { get; set; }
        public object yield7DayDate { get; set; }
        public object navChgYtdPct { get; set; }
        public object nav52WkHi { get; set; }
        public object nav52WkLo { get; set; }
        public object etf { get; set; }
        public object nav52WkHiDt { get; set; }
        public object nav52WkLoDt { get; set; }
        public object exchangeName { get; set; }
        public object invProgram { get; set; }
        public object rebalanceFreq { get; set; }
        public object indexProvider { get; set; }
        public object mgmtFee { get; set; }
        public object key { get; set; }
        public DisplayReturns displayReturns { get; set; }
        public object threeYearsData { get; set; }
    }

    public class E3
    {
        public object seriesCd { get; set; }
        public object axisCd { get; set; }
        public object currency { get; set; }
        public object nav { get; set; }
        public object navDt { get; set; }
        public object navChgAmt { get; set; }
        public object navChgPct { get; set; }
        public object navChgInd { get; set; }
        public object merDt { get; set; }
        public object merPct { get; set; }
        public object inceptionDt { get; set; }
        public object mstarRtng { get; set; }
        public object mstarDt { get; set; }
        public object distFreq { get; set; }
        public Codes codes { get; set; }
        public object aggrAssets { get; set; }
        public object aggrAssetsDt { get; set; }
        public object yieldNet { get; set; }
        public object yieldNetDate { get; set; }
        public object yield7Day { get; set; }
        public object yield7DayDate { get; set; }
        public object navChgYtdPct { get; set; }
        public object nav52WkHi { get; set; }
        public object nav52WkLo { get; set; }
        public object etf { get; set; }
        public object nav52WkHiDt { get; set; }
        public object nav52WkLoDt { get; set; }
        public object exchangeName { get; set; }
        public object invProgram { get; set; }
        public object rebalanceFreq { get; set; }
        public object indexProvider { get; set; }
        public object mgmtFee { get; set; }
        public object key { get; set; }
        public DisplayReturns displayReturns { get; set; }
        public object threeYearsData { get; set; }
    }

    public class E4T5
    {
        public object seriesCd { get; set; }
        public object axisCd { get; set; }
        public object currency { get; set; }
        public object nav { get; set; }
        public object navDt { get; set; }
        public object navChgAmt { get; set; }
        public object navChgPct { get; set; }
        public object navChgInd { get; set; }
        public object merDt { get; set; }
        public object merPct { get; set; }
        public object inceptionDt { get; set; }
        public object mstarRtng { get; set; }
        public object mstarDt { get; set; }
        public object distFreq { get; set; }
        public Codes codes { get; set; }
        public object aggrAssets { get; set; }
        public object aggrAssetsDt { get; set; }
        public object yieldNet { get; set; }
        public object yieldNetDate { get; set; }
        public object yield7Day { get; set; }
        public object yield7DayDate { get; set; }
        public object navChgYtdPct { get; set; }
        public object nav52WkHi { get; set; }
        public object nav52WkLo { get; set; }
        public object etf { get; set; }
        public object nav52WkHiDt { get; set; }
        public object nav52WkLoDt { get; set; }
        public object exchangeName { get; set; }
        public object invProgram { get; set; }
        public object rebalanceFreq { get; set; }
        public object indexProvider { get; set; }
        public object mgmtFee { get; set; }
        public object key { get; set; }
        public DisplayReturns displayReturns { get; set; }
        public object threeYearsData { get; set; }
    }

    public class F8
    {
        public object seriesCd { get; set; }
        public object axisCd { get; set; }
        public object currency { get; set; }
        public object nav { get; set; }
        public object navDt { get; set; }
        public object navChgAmt { get; set; }
        public object navChgPct { get; set; }
        public object navChgInd { get; set; }
        public object merDt { get; set; }
        public object merPct { get; set; }
        public object inceptionDt { get; set; }
        public object mstarRtng { get; set; }
        public object mstarDt { get; set; }
        public object distFreq { get; set; }
        public Codes codes { get; set; }
        public object aggrAssets { get; set; }
        public object aggrAssetsDt { get; set; }
        public object yieldNet { get; set; }
        public object yieldNetDate { get; set; }
        public object yield7Day { get; set; }
        public object yield7DayDate { get; set; }
        public object navChgYtdPct { get; set; }
        public object nav52WkHi { get; set; }
        public object nav52WkLo { get; set; }
        public object etf { get; set; }
        public object nav52WkHiDt { get; set; }
        public object nav52WkLoDt { get; set; }
        public object exchangeName { get; set; }
        public object invProgram { get; set; }
        public object rebalanceFreq { get; set; }
        public object indexProvider { get; set; }
        public object mgmtFee { get; set; }
        public object key { get; set; }
        public DisplayReturns displayReturns { get; set; }
        public object threeYearsData { get; set; }
    }

    public class T8
    {
        public object seriesCd { get; set; }
        public object axisCd { get; set; }
        public object currency { get; set; }
        public object nav { get; set; }
        public object navDt { get; set; }
        public object navChgAmt { get; set; }
        public object navChgPct { get; set; }
        public object navChgInd { get; set; }
        public object merDt { get; set; }
        public object merPct { get; set; }
        public object inceptionDt { get; set; }
        public object mstarRtng { get; set; }
        public object mstarDt { get; set; }
        public object distFreq { get; set; }
        public Codes codes { get; set; }
        public object aggrAssets { get; set; }
        public object aggrAssetsDt { get; set; }
        public object yieldNet { get; set; }
        public object yieldNetDate { get; set; }
        public object yield7Day { get; set; }
        public object yield7DayDate { get; set; }
        public object navChgYtdPct { get; set; }
        public object nav52WkHi { get; set; }
        public object nav52WkLo { get; set; }
        public object etf { get; set; }
        public object nav52WkHiDt { get; set; }
        public object nav52WkLoDt { get; set; }
        public object exchangeName { get; set; }
        public object invProgram { get; set; }
        public object rebalanceFreq { get; set; }
        public object indexProvider { get; set; }
        public object mgmtFee { get; set; }
        public object key { get; set; }
        public DisplayReturns displayReturns { get; set; }
        public object threeYearsData { get; set; }
    }

    public class S5
    {
        public object seriesCd { get; set; }
        public object axisCd { get; set; }
        public object currency { get; set; }
        public object nav { get; set; }
        public object navDt { get; set; }
        public object navChgAmt { get; set; }
        public object navChgPct { get; set; }
        public object navChgInd { get; set; }
        public object merDt { get; set; }
        public object merPct { get; set; }
        public object inceptionDt { get; set; }
        public object mstarRtng { get; set; }
        public object mstarDt { get; set; }
        public object distFreq { get; set; }
        public Codes codes { get; set; }
        public object aggrAssets { get; set; }
        public object aggrAssetsDt { get; set; }
        public object yieldNet { get; set; }
        public object yieldNetDate { get; set; }
        public object yield7Day { get; set; }
        public object yield7DayDate { get; set; }
        public object navChgYtdPct { get; set; }
        public object nav52WkHi { get; set; }
        public object nav52WkLo { get; set; }
        public object etf { get; set; }
        public object nav52WkHiDt { get; set; }
        public object nav52WkLoDt { get; set; }
        public object exchangeName { get; set; }
        public object invProgram { get; set; }
        public object rebalanceFreq { get; set; }
        public object indexProvider { get; set; }
        public object mgmtFee { get; set; }
        public object key { get; set; }
        public DisplayReturns displayReturns { get; set; }
        public object threeYearsData { get; set; }
    }

    public class P1T5
    {
        public object seriesCd { get; set; }
        public object axisCd { get; set; }
        public object currency { get; set; }
        public object nav { get; set; }
        public object navDt { get; set; }
        public object navChgAmt { get; set; }
        public object navChgPct { get; set; }
        public object navChgInd { get; set; }
        public object merDt { get; set; }
        public object merPct { get; set; }
        public object inceptionDt { get; set; }
        public object mstarRtng { get; set; }
        public object mstarDt { get; set; }
        public object distFreq { get; set; }
        public Codes codes { get; set; }
        public object aggrAssets { get; set; }
        public object aggrAssetsDt { get; set; }
        public object yieldNet { get; set; }
        public object yieldNetDate { get; set; }
        public object yield7Day { get; set; }
        public object yield7DayDate { get; set; }
        public object navChgYtdPct { get; set; }
        public object nav52WkHi { get; set; }
        public object nav52WkLo { get; set; }
        public object etf { get; set; }
        public object nav52WkHiDt { get; set; }
        public object nav52WkLoDt { get; set; }
        public object exchangeName { get; set; }
        public object invProgram { get; set; }
        public object rebalanceFreq { get; set; }
        public object indexProvider { get; set; }
        public object mgmtFee { get; set; }
        public object key { get; set; }
        public DisplayReturns displayReturns { get; set; }
        public object threeYearsData { get; set; }
    }

    public class P5
    {
        public object seriesCd { get; set; }
        public object axisCd { get; set; }
        public object currency { get; set; }
        public object nav { get; set; }
        public object navDt { get; set; }
        public object navChgAmt { get; set; }
        public object navChgPct { get; set; }
        public object navChgInd { get; set; }
        public object merDt { get; set; }
        public object merPct { get; set; }
        public object inceptionDt { get; set; }
        public object mstarRtng { get; set; }
        public object mstarDt { get; set; }
        public object distFreq { get; set; }
        public Codes codes { get; set; }
        public object aggrAssets { get; set; }
        public object aggrAssetsDt { get; set; }
        public object yieldNet { get; set; }
        public object yieldNetDate { get; set; }
        public object yield7Day { get; set; }
        public object yield7DayDate { get; set; }
        public object navChgYtdPct { get; set; }
        public object nav52WkHi { get; set; }
        public object nav52WkLo { get; set; }
        public object etf { get; set; }
        public object nav52WkHiDt { get; set; }
        public object nav52WkLoDt { get; set; }
        public object exchangeName { get; set; }
        public object invProgram { get; set; }
        public object rebalanceFreq { get; set; }
        public object indexProvider { get; set; }
        public object mgmtFee { get; set; }
        public object key { get; set; }
        public DisplayReturns displayReturns { get; set; }
        public object threeYearsData { get; set; }
    }

    public class S8
    {
        public object seriesCd { get; set; }
        public object axisCd { get; set; }
        public object currency { get; set; }
        public object nav { get; set; }
        public object navDt { get; set; }
        public object navChgAmt { get; set; }
        public object navChgPct { get; set; }
        public object navChgInd { get; set; }
        public object merDt { get; set; }
        public object merPct { get; set; }
        public object inceptionDt { get; set; }
        public object mstarRtng { get; set; }
        public object mstarDt { get; set; }
        public object distFreq { get; set; }
        public Codes codes { get; set; }
        public object aggrAssets { get; set; }
        public object aggrAssetsDt { get; set; }
        public object yieldNet { get; set; }
        public object yieldNetDate { get; set; }
        public object yield7Day { get; set; }
        public object yield7DayDate { get; set; }
        public object navChgYtdPct { get; set; }
        public object nav52WkHi { get; set; }
        public object nav52WkLo { get; set; }
        public object etf { get; set; }
        public object nav52WkHiDt { get; set; }
        public object nav52WkLoDt { get; set; }
        public object exchangeName { get; set; }
        public object invProgram { get; set; }
        public object rebalanceFreq { get; set; }
        public object indexProvider { get; set; }
        public object mgmtFee { get; set; }
        public object key { get; set; }
        public DisplayReturns displayReturns { get; set; }
        public object threeYearsData { get; set; }
    }

    public class Series
    {
    public A A { get; set; }
    public A T5 { get; set; }
    public A T8 { get; set; }
    public A B { get; set; }
    public A S5 { get; set; }
    public A S8 { get; set; }
    public A C { get; set; }
    public A D { get; set; }
    public A E1 { get; set; }
    public A E2 { get; set; }
    public A E3 { get; set; }
    public A E4 { get; set; }
    public A E5 { get; set; }
    public A E1T5 { get; set; }
    public A E2T5 { get; set; }
    public A E3T5 { get; set; }
    public A E4T5 { get; set; }
    public A E5T5 { get; set; }
    public A F { get; set; }
    public A F5 { get; set; }
    public A F8 { get; set; }
    public A I { get; set; }
    public A I5 { get; set; }
    public A I8 { get; set; }
    public A L { get; set; }
    public P2 P2 { get; set; }
    public P3T5 P3T5 { get; set; }
    public P2T5 P2T5 { get; set; }
    public P1 P1 { get; set; }
    public P3 P3 { get; set; }
    public P4 P4 { get; set; }
    public P4T5 P4T5 { get; set; }
    public P1T5 P1T5 { get; set; }
    public P5 P5 { get; set; }
}

    public class Allocations
    {
        [JsonPropertyName("Foreign Equities")]
        public object ForeignEquities { get; set; }

        [JsonPropertyName("Canadian Equities")]
        public object CanadianEquities { get; set; }
        public object Convertibles { get; set; }

        [JsonPropertyName("Other Investments")]
        public object OtherInvestments { get; set; }

        [JsonPropertyName("Information Technology")]
        public object InformationTechnology { get; set; }
        public object Materials { get; set; }
        public object Industrials { get; set; }

        [JsonPropertyName("Consumer Disc.")]
        public object ConsumerDisc { get; set; }

        [JsonPropertyName("Health Care")]
        public object HealthCare { get; set; }
        public object Financials { get; set; }

        [JsonPropertyName("Communication Services")]
        public object CommunicationServices { get; set; }
        public object Energy { get; set; }
        public object Utilities { get; set; }

        [JsonPropertyName("Consumer Staples")]
        public object ConsumerStaples { get; set; }

        [JsonPropertyName("Real Estate")]
        public object RealEstate { get; set; }

        [JsonPropertyName("United States")]
        public object UnitedStates { get; set; }
        public object Canada { get; set; }
        public object Brazil { get; set; }

        [JsonPropertyName("United Kingdom")]
        public object UnitedKingdom { get; set; }
        public object Denmark { get; set; }
        public object France { get; set; }

        [JsonPropertyName("Other Countries")]
        public object OtherCountries { get; set; }
    }

    public class CM
    {
        public object asOfDate { get; set; }
        public object period { get; set; }
        public Allocations allocations { get; set; }
    }

    public class LM
    {
        public object asOfDate { get; set; }
        public object period { get; set; }
        public Allocations allocations { get; set; }
    }

    public class TMA
    {
        public object asOfDate { get; set; }
        public object period { get; set; }
        public Allocations allocations { get; set; }
    }

    public class AssetAllocations
    {
        public CM CM { get; set; }
        public CM LM { get; set; }
        public CM TMA { get; set; }
    }

    public class SectorAllocations
    {
        public CM CM { get; set; }
        public LM LM { get; set; }
        public TMA TMA { get; set; }
    }

    public class CountryAllocations
    {
        public CM CM { get; set; }
        public CM LM { get; set; }
        public CM TMA { get; set; }
    }

    public class AllocationsObject
    {
        public AssetAllocations assetAllocations { get; set; }
        public AssetAllocations sectorAllocations { get; set; }
        public AssetAllocations countryAllocations { get; set; }
        public AssetAllocations creditQualityAllocations { get; set; }
        public AssetAllocations targetAllocations { get; set; }
    }



    public class HoldingsByDateObj
{
        public object totalHoldings { get; set; }
        public object totalIssuers { get; set; }
        public object topHoldingsAggrPct { get; set; }
        public object topIssuersAggrPct { get; set; }
        public List<object> topHoldingsCompanies { get; set; }
        public object shortHoldings { get; set; }
        public object longHoldings { get; set; }
        public object hasPositionTypeHoldings { get; set; }
    }

    public class HoldingsByDate
    {
        [JsonPropertyName("30-Jun-2021")]
        public HoldingsByDateObj _30Jun2021 { get; set; }

        [JsonPropertyName("31-Mar-2021")]
        public HoldingsByDateObj _31Mar2021 { get; set; }

        [JsonPropertyName("31-Dec-2020")]
        public HoldingsByDateObj _31Dec2020 { get; set; }

        [JsonPropertyName("30-Sep-2020")]
        public HoldingsByDateObj _30Sep2020 { get; set; }
    }

    public class Holding
    {
        public object name { get; set; }
        public object marketValue { get; set; }
        public object netPercent { get; set; }
    }

    public class CashOther
    {
        public object asOfDate { get; set; }
        public object categoryName { get; set; }
        public List<Holding> holdings { get; set; }
    }

    public class CanadianCorporateBonds
    {
        public object asOfDate { get; set; }
        public object categoryName { get; set; }
        public List<Holding> holdings { get; set; }
    }

    public class CanadianEquities
    {
        public object asOfDate { get; set; }
        public object categoryName { get; set; }
        public List<Holding> holdings { get; set; }
    }

    public class Convertibles
    {
        public object asOfDate { get; set; }
        public object categoryName { get; set; }
        public List<Holding> holdings { get; set; }
    }

    public class ForeignEquities
    {
        public object asOfDate { get; set; }
        public object categoryName { get; set; }
        public List<Holding> holdings { get; set; }
    }

    public class HoldingsByCategory
    {
        [JsonPropertyName("Cash & Other")]
        public CashOther CashOther { get; set; }

        [JsonPropertyName("Canadian Corporate Bonds")]
        public CanadianCorporateBonds CanadianCorporateBonds { get; set; }

        [JsonPropertyName("Canadian Equities")]
        public CanadianEquities CanadianEquities { get; set; }
        public Convertibles Convertibles { get; set; }

        [JsonPropertyName("Foreign Equities")]
        public ForeignEquities ForeignEquities { get; set; }
    }

    public class HoldingsObject
    {
        public HoldingsByDate holdingsByDate { get; set; }
        public HoldingsByCategory holdingsByCategory { get; set; }
    }

    public class AnnStdDev
    {
        public object fund { get; set; }
        public object benchmark { get; set; }
    }

    public class Beta
    {
        public object fund { get; set; }
        public object benchmark { get; set; }
    }

    public class Rsquared
    {
        public object fund { get; set; }
        public object benchmark { get; set; }
    }

    public class Risk
    {
        public object asOfDate { get; set; }
        public object hideBeta { get; set; }
        public AnnStdDev annStdDev { get; set; }
        public Beta beta { get; set; }
        public Rsquared rsquared { get; set; }
    }

    public class DisplayReturns
    {
        public object ADV { get; set; }
        public object PUB { get; set; }
    }

    public class Options
    {
        public object has3YearsRiskData { get; set; }
        public Series series { get; set; }
    }

    public class Neutral
    {
        public object axisCode { get; set; }
        public object url { get; set; }
    }

    public class Us
    {
        public object axisCode { get; set; }
        public object url { get; set; }
    }

    public class TswpF5
    {
        public object axisCode { get; set; }
        public object url { get; set; }
    }

    public class TswpS5
    {
        public object axisCode { get; set; }
        public object url { get; set; }
    }

    public class Related
    {
        public Neutral neutral { get; set; }
        public Us us { get; set; }
        public TswpF5 tswpF5 { get; set; }
        public TswpS5 tswpS5 { get; set; }
    }

    public class CAD
    {
        public object lbl { get; set; }
        public object fund { get; set; }
        public object mktPrc { get; set; }
        public object qrt { get; set; }
        public object bench { get; set; }
    }

    public class USD
    {
        public object lbl { get; set; }
        public object fund { get; set; }
        public object mktPrc { get; set; }
        public object qrt { get; set; }
        public object bench { get; set; }
    }

    public class Currency
    {
        public List<CAD> CAD { get; set; }
        public List<USD> USD { get; set; }
    }

    public class ReturnsCalendar
    {
        public object dt { get; set; }
        public Series series { get; set; }
    }

    public class ReturnsCompound
    {
        public object dt { get; set; }
        public Series series { get; set; }
    }

    public class RootFundInfo
    {
        public object asOfDate { get; set; }
        public object inceptionDate { get; set; }
        public object axisCode { get; set; }
        public Series series { get; set; }
        public object yield7dayDt { get; set; }
        public object yield7day { get; set; }
        public object yield { get; set; }
        public object durationYrs { get; set; }
        public object durationDt { get; set; }
        public object mstarCat { get; set; }
        public object volatility { get; set; }
        public AllocationsObject allocations { get; set; }
        public HoldingsObject holdings { get; set; }
        public object performance { get; set; }
        public Risk risk { get; set; }
        public Options options { get; set; }
        public Related related { get; set; }
        public object qfr { get; set; }
        public object hedgeRatioDt { get; set; }
        public object hedgeRatio { get; set; }
        public object exposures { get; set; }
        public object etf { get; set; }
        public ReturnsCalendar returns_calendar { get; set; }
        public ReturnsCompound returns_compound { get; set; }
    }

