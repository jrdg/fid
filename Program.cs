﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using CsvHelper;
using CsvHelper.Configuration;
using System.Linq;
using System.Text.Json;
using System.Text.Json.Serialization;
using System.Dynamic;

namespace vscode_playground
{

    public class Catgri
    {
        public string catgry { get; set; }
        public string subCatgry { get; set; }
    }

    public class FundInfo
    {
        public string code { get; set; }
        public string axisCd { get; set; }
        public string name { get; set; }
        public string longNm { get; set; }
        public string oldNm { get; set; }
        public string curr { get; set; }
        public string series { get; set; }
        public string chrgOptn { get; set; }
        public string url { get; set; }
        public string tSWP { get; set; }
        public string inceptionDate { get; set; }
        public string corpType { get; set; }
        public string dfltSubCatgry { get; set; }
        public List<Catgri> catgries { get; set; }
    }

    public class FundsRoot
    {
        public List<FundInfo> fundInfo { get; set; } 
    }

    public class Fund
    {
        private Dictionary<string, HoldingByDate> TopHoldings;

        public Fund()
        {
            TopHoldings = new Dictionary<string, HoldingByDate>();
        }

        public void AddTopHolding(string date, HoldingByDate holdings)
        {
            TopHoldings.Add(date, holdings);
        }
    }

    public class HoldingByDate
    {
        public string totalHoldings { get; set; }
        public string totalIssuers { get; set; }
        public string topHoldingsAggrPct { get; set; }
        public string topIssuersAggrPct { get; set; }
        public List<string> topHoldingsCompanies { get; set; }
        public string shortHoldings { get; set; }
        public string longHoldings { get; set; }
        public bool hasPositionTypeHoldings { get; set; }
    }

    //https://www.fidelity.ca/sites/Satellite?pagename=Fidelity/API/FundReference&locale=en_CA pour trouver toute les series possible, top fonds de fidelity, le nom des categories etc...

    class Program
    {
        public static bool HasProperty(dynamic obj, string name)
        {
            Type objType = obj.GetType();

            if (objType == typeof(ExpandoObject))
            {
                return ((IDictionary<string, object>)obj).ContainsKey(name);
            }

            return objType.GetProperty(name) != null;
        }

        public static async Task<RootFundInfo> GetFundInfo(string axisCd)
        {
            try
            {
                var jsonFundInfoPage = await HttpHelper._httpClient.GetStringAsync($"https://www.fidelity.ca/sites/Satellite?pagename=Fidelity/FundPage/FundData&locale=fr_CA&code={axisCd}");
                var fundInfoPage = JsonSerializer.Deserialize<RootFundInfo>(jsonFundInfoPage);
                return fundInfoPage;
            }
            catch(Exception e)
            {
                throw new Exception($"Un probleme est survenue dans la method GetFundInfo(string axisCd = {axisCd}) => {e.Message}");
            }
        }

        public static async Task<RootFidelityFundChart> GetFundChart(string axisCd, string series)
        {
            try
            {
                var jsonFundInfoPage = await HttpHelper._httpClient.GetStringAsync($"https://www.fidelity.ca/sites/Satellite?pagename=Fidelity/API/DailyPrice&fundcode={axisCd}&series={series}&locale=fr_CA");
                var fundInfoPage = JsonSerializer.Deserialize<RootFidelityFundChart>(jsonFundInfoPage);
                return fundInfoPage;
            }
            catch (Exception e)
            {
                throw new Exception($"Un probleme est survenue dans la method GetFundChart(string axisCd = {axisCd}, string series = {series}) => {e.Message}");
            }
        }

        public static async Task<RootFidelityInfo> GetFundFidelityInfos()
        {
            try
            {
                var jsonFidelityInfo = await HttpHelper._httpClient.GetStringAsync("https://www.fidelity.ca/sites/Satellite?pagename=Fidelity/API/FundReference&locale=fr_CA");
                var fidelityInfos = JsonSerializer.Deserialize<RootFidelityInfo>(jsonFidelityInfo);
                return fidelityInfos;
            }
            catch(Exception e)
            {
                throw new Exception($"Un probleme est survenue dans la method GetFundFidelityInfos() => {e.Message}");
            }
        }

        public static async Task<FundsRoot> GetFundsRoot()
        {
            try
            {
                var json = await HttpHelper._httpClient.GetStringAsync("https://www.fidelity.ca/sites/Satellite?pagename=Fidelity/API/FundInfo&locale=fr_CA");
                var funds = JsonSerializer.Deserialize<FundsRoot>(json);
                return funds;
            }
            catch (Exception e)
            {
                throw new Exception($"Un probleme est survenue dans la method GetFundsRoot() => {e.Message}");
            }
        }

        public static async Task<FidelitySingleFundData> GetFundData(string axisCode, string serie)
        {
            try
            {
                // get les infos du fond exemple le prix
                var fundInfoTask = GetFundInfo(axisCode);

                // get la chart du fond (prix par jour)
                var fundChartTask = GetFundChart(axisCode, serie);

                await Task.WhenAll(fundInfoTask, fundChartTask);

                return new FidelitySingleFundData
                {
                    FundInfo = await fundInfoTask,
                    FundChartData = await fundChartTask
                };
            }
            catch(Exception e)
            {
                throw new Exception($"Un probleme est survenue lors de l'appel de la methode GetFundData(string axisCode = {axisCode}, string serie = {serie}) => {e.Message}");
            }
        }

        public class FidelitySingleFundData
        {
            public RootFundInfo FundInfo { get; set; }
            public RootFidelityFundChart FundChartData { get; set; }
        }

        static async Task  Main(string[] args)
        {

            RootFidelityInfo fundFidelityInfos = null;
            FundsRoot fundsRoot = null;

            try
            {
                // get les infos relatives au fonds
                fundFidelityInfos = await GetFundFidelityInfos();
            }
            catch(Exception e)
            {
                // log l'erreur;
            }


            try
            {
                // get les infos relative au fond specifiques
                fundsRoot = await GetFundsRoot();
            }
            catch(Exception e)
            {
                // log l'erreur
            }

            if(fundsRoot != null)
            {
                var fetchingTasks = new List<Task<FidelitySingleFundData>>();

                var axisCodeOffert = new List<string> { "cg", "ugg", "gin", "gbp", "uet", "gc", "cfe", "os", "ss" };

                var fondOffertParGsfChezFidelityGrouperParAxisCode = fundsRoot.fundInfo
                    .Where(x => axisCodeOffert.Contains(x.axisCd.ToLower()))
                    .GroupBy(x => x.axisCd);

                axisCodeOffert.ForEach(code =>
                {
                    var fondsDuAxisCode = fondOffertParGsfChezFidelityGrouperParAxisCode.FirstOrDefault(x => x.Key.ToLower() == code.ToLower());

                    if(fondsDuAxisCode == null)
                    {
                        // log l'erreur comme quoi la list de fond du axis code courant est null
                        Console.WriteLine($"la list de fond du axis code {code} est null");
                    }
                    else if(fondsDuAxisCode.Count() == 0)
                    {
                        // log l'erreur comme quoi la list de fonds du axis code courant ne contient aucun element.
                        Console.WriteLine($"la list de fonds du axis code {code} ne contient aucun element.");
                    }
                });

                foreach (var fundGroup in fondOffertParGsfChezFidelityGrouperParAxisCode)
                {
                    foreach (var fund in fundGroup)
                    {
                        fetchingTasks.Add(GetFundData(fund.axisCd, fund.series));
                    }
                }

                await Task.WhenAll(fetchingTasks);
            }


            /*var fileName = "profile_bulk.csv";
            var lines = await File.ReadAllLinesAsync(fileName);

            var config = new CsvConfiguration(CultureInfo.InvariantCulture)
            {
                PrepareHeaderForMatch = args => args.Header.ToUpper()
            };

            using (var reader = new StreamReader(fileName))
            using (var csv = new CsvReader(reader, config))
            {
                var records = csv.GetRecords<FMPCompanyProfile>().Where(x => !x.DefaultImage);
                Console.WriteLine(records.Count());
                var t = new List<Task<string>>();*/

            // foreach(var r in records)
            //{
            //  var split = r.Image.Split(".");
            // var extension = split[split.Length-1];
            // t.Add(HttpHelper.DownloadFileAsync(r.Image, $"../jorda/company_image_{r.Symbol.Replace(".","").Replace("^","")}.{extension}"));
            // }

            //await Task.WhenAll(t);

            //var list = new List<string>();

            //foreach(var ts in t)
            //{
            // list.Add(await ts);
            //}

            // await File.AppendAllLinesAsync("cantread.txt", list);

        }
        }
    }

public static class HttpHelper
{
   public static HttpClientHandler clientHandler = new HttpClientHandler();
   public static readonly HttpClient _httpClient;

   static HttpHelper()
   {
        clientHandler.ServerCertificateCustomValidationCallback = (sender, cert, chain, sslPolicyErrors) => { return true; };
       _httpClient = new HttpClient(clientHandler);
        _httpClient.Timeout = TimeSpan.FromSeconds(1000);
   }

   public static async Task<string> DownloadFileAsync(string uri, string outputPath)
   {
      Uri uriResult;

      if (!Uri.TryCreate(uri, UriKind.Absolute, out uriResult))
         throw new InvalidOperationException("URI is invalid.");

    bool success = true;
    string str = $"{uri}";
    Console.WriteLine($"starting download {uri}");

    try
    {
        byte[] fileBytes = await _httpClient.GetByteArrayAsync(uri);
        Console.WriteLine($"FINISH download {uri}");
        File.WriteAllBytes(outputPath, fileBytes);
    }
    catch(Exception e)
    {
       success = false;
       str += $" {success} => {e.Message}";
       //File.AppendAllLines("cantread.txt", new List<string> {$"{uri} message => {e.Message}"});
    }

     return str;
   }
}