﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace vscode_playground
{
    // Root myDeserializedClass = JsonConvert.DeserializeObject<Root>(myJsonResponse); 
    public class FundSeriesLoad
    {
        public string series { get; set; }
        public string load { get; set; }
        public string TSWP { get; set; }
    }

    public class FundSeriesLoadWithO
    {
        public string series { get; set; }
        public string load { get; set; }
        public string TSWP { get; set; }
    }

    public class FundSeriesGroups
    {
        public List<string> L { get; set; }
        public List<string> F { get; set; }
        public List<string> B { get; set; }
        public List<string> A { get; set; }
        public List<string> P { get; set; }
        public List<string> E { get; set; }
        public List<string> I { get; set; }
        public List<string> C { get; set; }
        public List<string> D { get; set; }
        public List<string> tswp { get; set; }
    }

    public class FundCurrency
    {
        public string curr { get; set; }
        public string name { get; set; }
    }

    public class FundCategory
    {
        public string catgry { get; set; }
        public string name { get; set; }
    }

    public class FundSubCategory
    {
        public string catgry { get; set; }
        public string name { get; set; }
    }

    public class TopFund
    {
        public string axisCd { get; set; }
        public string name { get; set; }
    }

    public class RootFidelityInfo
    {
        public List<FundSeriesLoad> fundSeriesLoad { get; set; }
        public List<FundSeriesLoadWithO> fundSeriesLoadWithO { get; set; }
        public FundSeriesGroups fundSeriesGroups { get; set; }
        public List<FundCurrency> fundCurrencies { get; set; }
        public List<FundCategory> fundCategories { get; set; }
        public List<FundSubCategory> fundSubCategories { get; set; }
        public List<TopFund> topFunds { get; set; }
    }
}
